
#ifndef _DRIVER_WIFI_H_
#define _DRIVER_WIFI_H_

//#include "manage_variable.h"
//#include "F2806x_Sci.h"
#include "stdint.h"
#include "gd32f30x.h"

#define RXSIZE          150
#define TXSIZE          150
#define RXTIMEOUT       10   //ms
#define TXTIMEOUT       10   //ms

typedef struct
{
    uint16_t start_addr;
    void *data;
    uint16_t len;
}DataModBus;

void ModbusInit(void);
void ModbusAddVariable(uint16_t addr, void *data, uint16_t len);

#endif /* _DRIVER_WIFI_H_ */
