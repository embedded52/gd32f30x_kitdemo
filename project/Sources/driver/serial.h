
#ifndef _DRIVER_SERIAL_H_
#define _DRIVER_SERIAL_H_

#include "stdint.h"


void WifiRs485Init(void);
void WifiRs485Send(char s[20]);
void WifiRs485SendChar(char a);
void WifiRs485EnableTx(void);
void WifiRs485DisableTx(void);


#endif /* _DRIVER_SERIAL_H_ */
