#ifndef UART_H
#define UART_H

#include "stdint.h"
#include "gd32f30x.h"
#include "gd32f30x_usart.h"
#include "types.h"
#include "queue.h"

#define NAME_MAX_LEN 		10
#define RESET_VALUE 		0x00
#define ARRAYNUM(arr_nanme)      (uint32_t)(sizeof(arr_nanme) / sizeof(*(arr_nanme)))
//#define TRANSMIT_SIZE            (ARRAYNUM(txbuffer) - 1)
typedef enum
{
	FUNC_NO_ERR = 0,
	FUNC_INIT_FAIL,
	FUNC_RECV_FAIL,
	FUNC_SEND_FAIL,
	FUNC_TIMEOUT,
	FUNC_FAIL
}FUNC_RET_E;

typedef struct SerialObj
{
	uint8_t 				ID;
	const char* 			Tag;
	uint16_t 				rx_count;
	uint32_t				Baudrate;
	uint8_t					byteSize;
	uint8_t					stopBit;
	uint8_t					parity;
	uint8_t					flowControl;
	bool					rxCpltFlg;
	QUEUE_NODE_T*			recv_queue;
	bool 					isCreated;
	FUNC_RET_E 	(*send)(struct SerialObj* obj,uint8_t* data , uint16_t len );
	uint16_t 	(*recv)(struct SerialObj* obj,uint8_t* data , uint16_t len );
	void 		(*handle)(struct SerialObj* obj );
	uint16_t    (*recvBlocking)(struct SerialObj* obj,uint8_t* data , uint16_t len);
}SerialObj;


SerialObj* SerialPort_UartInit( uint8_t ID, const char* tag,uint16_t rx_size);
bool SerialPort_UartDeInit(SerialObj* obj);
FUNC_RET_E SerialPort_UartSend(SerialObj* obj,uint8_t* data , uint16_t len );
bool SerialPort_IsDoneFrame(SerialObj* obj);
uint16_t SerialPort_UartRecv(SerialObj* obj,uint8_t* data , uint16_t len );
void SerialPort_UartHandle(SerialObj* obj );
uint16_t SerialPort_UartRecvBlocking(SerialObj* obj,uint8_t* data , uint16_t len );

#endif 
