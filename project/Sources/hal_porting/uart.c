#include "uart.h"

/******************************************************************************
 * Constants and macros
 ******************************************************************************/
#define MAX_UART_OBJ    4
#define MAX_TIME_OUT    (0xFFFF)
#define UART_ERROR_EVENTS         (UART_EVENT_BREAK_DETECT | UART_EVENT_ERR_OVERFLOW | UART_EVENT_ERR_FRAMING | \
                                    UART_EVENT_ERR_PARITY)
																		
#define RX_TIMEOUT				3000

typedef struct
{
    uint8_t     priority;
    uint8_t channel;
    const char *name;
    int     baudrate;
    uint32_t tick;
		uint32_t 								giga_uart_reg;
		uint32_t								giga_gpio_reg;
    rcu_periph_enum    			gpio_periph;
    rcu_periph_enum         uart_periph;
		uint32_t 								giga_tx_pin;
		uint32_t								giga_rx_pin;
    SerialObj                   *serial_obj;
    volatile uint8_t                    *Event_intr;
		IRQn_Type								irqType;
}GIGA_SERIAL;

/******************************************************************************
 * Local function prototypes
 ******************************************************************************/

/******************************************************************************
 * Local variables
 ******************************************************************************/
static volatile uint8_t s_uart1Event =  RESET_VALUE;
static volatile uint8_t s_uart2Event  =  RESET_VALUE;
static volatile uint8_t s_uart3Event  =  RESET_VALUE;
static volatile uint8_t s_uart4Event =  RESET_VALUE;

static GIGA_SERIAL serial_list[MAX_UART_OBJ] = {

    {4,0,"PC",115200,0,USART0,GPIOA,RCU_GPIOA,RCU_USART0,GPIO_PIN_9,GPIO_PIN_10,NULL,&s_uart2Event,USART0_IRQn},
		{4,1,"UART1",115200,0,USART1,GPIOA,RCU_GPIOA,RCU_USART1,GPIO_PIN_2,GPIO_PIN_3,NULL,&s_uart1Event,USART1_IRQn},

};
static volatile uint8_t s_uartTableSize = ARRAYNUM(serial_list);

SerialObj* SerialPort_UartInit( uint8_t ID, const char* tag,uint16_t rx_size )
{

    uint8_t j;
    SerialObj* obj = malloc(sizeof(SerialObj));
    memset(obj, 0,sizeof(SerialObj));
    if(NULL == obj)
    {
        return NULL;
    }
    obj->ID =  ID;
    obj->Tag = tag;
    obj->recv_queue = QUEUE_InitQueue(rx_size, sizeof(uint8_t));
    if(obj->recv_queue == NULL)
    {
        free(obj);
        return NULL;
    }
    obj->rxCpltFlg = false;
    obj->rx_count = 0;
    obj->recv = SerialPort_UartRecv;
    obj->send = SerialPort_UartSend;
    obj->handle = SerialPort_UartHandle;
    obj->recvBlocking = SerialPort_UartRecvBlocking;
    for( j = 0; j < s_uartTableSize; j++)
    {
        if(serial_list[j].channel == ID)
        {
						nvic_irq_enable(serial_list[j].irqType, serial_list[j].priority, serial_list[j].priority);
            /* enable GPIO clock */
						rcu_periph_clock_enable(serial_list[j].gpio_periph);

						/* enable USART clock */
						rcu_periph_clock_enable(serial_list[j].uart_periph);

						/* connect port to USARTx_Tx */
						gpio_init(serial_list[j].giga_gpio_reg, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, serial_list[j].giga_tx_pin);

						/* connect port to USARTx_Rx */
						gpio_init(serial_list[j].giga_gpio_reg, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, serial_list[j].giga_rx_pin);

						/* USART configure */
						usart_deinit(serial_list[j].giga_uart_reg);
						usart_baudrate_set(serial_list[j].giga_uart_reg, serial_list[j].baudrate);
						usart_receive_config(serial_list[j].giga_uart_reg, USART_RECEIVE_ENABLE);
						usart_transmit_config(serial_list[j].giga_uart_reg, USART_TRANSMIT_ENABLE);
					
					  
						usart_interrupt_enable(serial_list[j].giga_uart_reg, USART_INT_RBNE);
					
						usart_enable(serial_list[j].giga_uart_reg);
					
					
            serial_list[j].serial_obj = obj;
           
            obj->isCreated = true;
            break;
        }
    }

    return obj;
}

void USART_Deinit(uint32_t com)
{
	usart_deinit(com);
}

/******************************************************************************
 * Local functions
 ******************************************************************************/

uint8_t txcount=0;
FUNC_RET_E SerialPort_UartSend(SerialObj* obj,uint8_t* data , uint16_t len )
{
    uint8_t j,k;
    uint32_t local_timeout = MAX_TIME_OUT;
		FUNC_RET_E ret = FUNC_NO_ERR;
    for( j = 0; j < s_uartTableSize; j++)
    {
        if( serial_list[j].channel == obj->ID )
        {
					for(k=0;k<len;k++){
            *serial_list[j].Event_intr = 0;
							
							usart_data_transmit(serial_list[j].giga_uart_reg, *(data+k));
							usart_interrupt_enable(serial_list[j].giga_uart_reg, USART_INT_TBE);
						//
           

            while ((USART_FLAG_TC != *(serial_list[j].Event_intr)) && (--local_timeout))
            {
                /* Check if any error event occurred */
                if (USART_FLAG_ORERR == *(serial_list[j].Event_intr))
                {
										ret = FUNC_SEND_FAIL;
										break;
                    //return FUNC_SEND_FAIL;
                }
            }
            if(RESET_VALUE == local_timeout)
            {
							ret = FUNC_TIMEOUT;
              //return FUNC_TIMEOUT;
            }
					}
				
					break;
        }
        *(serial_list[j].Event_intr) =  0;
    }
    return ret;
}

bool SerialPort_IsDoneFrame(SerialObj* obj)
{
	SerialPort_UartHandle(obj);
	return obj->rxCpltFlg;
}

uint16_t SerialPort_UartRecv(SerialObj* obj,uint8_t* data , uint16_t len )
{
    uint16_t ret = 0;

    while(!QUEUE_QueueIsEmpty(obj->recv_queue))
    {
        QUEUE_DeQueue(obj->recv_queue, data+ret);
        ret++;
        if(ret >= len)
        {
            break;
        }
    }
    obj->rxCpltFlg = false;
    obj->rx_count = 0;
    return ret;
}


void USART1_IRQHandler(void)
{
    uint8_t ret = usart_interrupt_flag_get(serial_list[1].giga_uart_reg, USART_INT_FLAG_RBNE);
		if(RESET != ret)
		{
			/* receive data */
			uint8_t data =(uint8_t) usart_data_receive(serial_list[1].giga_uart_reg);
			QUEUE_EnQueue(serial_list[1].serial_obj->recv_queue, &data);
			
		}
		 if(RESET != usart_interrupt_flag_get(serial_list[1].giga_uart_reg, USART_INT_FLAG_TBE)){
        /* transmit data */
        *(serial_list[1].Event_intr) = USART_FLAG_TC;
			 usart_interrupt_disable(serial_list[1].giga_uart_reg , USART_INT_TBE);
    }
        
     //   if(channel >= s_uartTableSize) return;
}


void SerialPort_UartHandle( SerialObj* obj)
{
		int j;
    for(j = 0; j < s_uartTableSize; j++)
    {
        if(serial_list[j].channel == obj->ID)
        {
                if(obj->recv_queue->current_count != 0)
                {
                    if( (obj->rx_count == obj->recv_queue->current_count) && (serial_list[j].tick > RX_TIMEOUT))
                    {
                        serial_list[j].tick = 0;
                        obj->rxCpltFlg = true;
                    }
                    else
                    {
                        obj->rx_count = obj->recv_queue->current_count;
                        serial_list[j].tick++;
                    }
                }
                else
                {
                    serial_list[j].tick = 0 ;
                }
            break;
        }
    }
}
    uint16_t ret = 0;
    uint32_t recv_len;
uint16_t SerialPort_UartRecvBlocking(SerialObj* obj,uint8_t* data , uint16_t len )
{
	  uint16_t ret = 0;
    uint32_t recv_len,index;
    recv_len = obj->recv_queue->current_count;
    index = obj->recv_queue->get_index;

    while(recv_len > 0)
    {
        if(index > ((obj->recv_queue->max_count / obj->recv_queue->node_size) - 1))
        {
            index = 0 ;
        }
        *(data + ret) = *((uint8_t*)obj->recv_queue->queue + (index * obj->recv_queue->node_size));
        ret++;
        index++;
        recv_len--;
        if(ret>len-1)
        {
					 obj->rxCpltFlg = false;
						obj->rx_count = 0;
            break;
        }
    }

    return ret;
}

bool SerialPort_UartDeInit(SerialObj* obj)
{
    
	
    usart_deinit(serial_list[obj->ID].giga_uart_reg);
    if(obj != null)
    {
        if(obj->recv_queue != null)
        {
            QUEUE_DestoryQueue(obj->recv_queue);
            obj->recv_queue = null;
        }
        free(obj);
        obj = null;
    }
    return true;
}


