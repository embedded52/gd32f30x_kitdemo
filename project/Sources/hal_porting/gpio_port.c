#include "gpio_port.h"
#include "gd32f30x.h"

typedef struct gpio_pin_match_t
{
    gpio_pin_e              ID;
    uint32_t       portType;
		uint32_t 			num_pin;
}gpio_pin_match_t;


gpio_pin_match_t gpio_pin[] =
{
 {PA0     	,GPIOA 				,GPIO_PIN_0},    //01
 {PA1     	,GPIOA				,GPIO_PIN_1},    //09
 {PA2     	,GPIOA				,GPIO_PIN_2},    //10
 {PA3     	,GPIOA				,GPIO_PIN_3},    //12
 {PA4     	,GPIOA				,GPIO_PIN_4},    //13
 {PA5     	,GPIOA				,GPIO_PIN_5},    //14
 {PA6    		,GPIOA				,GPIO_PIN_6},    //15
 {PA7    	 	,GPIOA				,GPIO_PIN_7},    //21
 {PA8     	,GPIOA				,GPIO_PIN_8},    //22
 {PA9     	,GPIOA				,GPIO_PIN_9},    //30
 {PA10     	,GPIOA				,GPIO_PIN_10},    //31
 {PA11     	,GPIOA				,GPIO_PIN_11},    //52
 {PA12     	,GPIOA				,GPIO_PIN_12},    //53
 {PA13     	,GPIOA				,GPIO_PIN_13},    //54
 {PA14     	,GPIOA				,GPIO_PIN_14},    //61
 {PA15     	,GPIOA				,GPIO_PIN_15},    //62
 {PB0     	,GPIOB 				,GPIO_PIN_0},    //01
 {PB1     	,GPIOB				,GPIO_PIN_1},    //09
 {PB2     	,GPIOB				,GPIO_PIN_2},    //10
 {PB3     	,GPIOB				,GPIO_PIN_3},    //12
 {PB4     	,GPIOB				,GPIO_PIN_4},    //13
 {PB5     	,GPIOB				,GPIO_PIN_5},    //14
 {PB6    		,GPIOB				,GPIO_PIN_6},    //15
 {PB7    	 	,GPIOB				,GPIO_PIN_7},    //21
 {PB8     	,GPIOB				,GPIO_PIN_8},    //22
 {PB9     	,GPIOB				,GPIO_PIN_9},    //30
 {PB10     	,GPIOB				,GPIO_PIN_10},    //31
 {PB11     	,GPIOB				,GPIO_PIN_11},    //52
 {PB12     	,GPIOB				,GPIO_PIN_12},    //53
 {PB13     	,GPIOB				,GPIO_PIN_13},    //54
 {PB14     	,GPIOB				,GPIO_PIN_14},    //61
 {PB15     	,GPIOB				,GPIO_PIN_15},    //62
 {PC0     	,GPIOC 				,GPIO_PIN_0},    //01
 {PC1     	,GPIOC				,GPIO_PIN_1},    //09
 {PC2     	,GPIOC				,GPIO_PIN_2},    //10
 {PC3     	,GPIOC				,GPIO_PIN_3},    //12
};

void GPIO_Init (gpio_pin_e gpio_pin)
{
	
    /* enable the led clock */
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_GPIOC);
	return;
}

void GPIOPORT_PinDir (gpio_pin_e pin, gpio_port_dir_e dir)
{
    
}
void GPIOPORT_PinWrite (gpio_pin_e pin,  gpio_port_level_e level)
{
	if(level == GPIO_LOW)
		{
			gpio_bit_reset(gpio_pin[pin].portType,gpio_pin[pin].num_pin);
    }
		else
		{
			gpio_bit_set(gpio_pin[pin].portType,gpio_pin[pin].num_pin);
		}
	return;
}
uint8_t GPIOPORT_PinRead (gpio_pin_e pin)
{
	return gpio_input_bit_get(gpio_pin[pin].portType,gpio_pin[pin].num_pin);
}
void GPIOPORT_PinToggle (gpio_pin_e pin)
{
	gpio_bit_write(gpio_pin[pin].portType,gpio_pin[pin].num_pin,(bit_status)(1-GPIOPORT_PinRead (pin)));
	return;
}

